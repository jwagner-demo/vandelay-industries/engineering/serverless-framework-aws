
image: node:14

default:
  tags: [] #leave blank unless directed otherwise in an instructor led classroom

variables:
  SECRET_DETECTION_HISTORIC_SCAN: "true"
  A_VARIABLE: "Testing config input"
  NO_ENV_BRANCH_PATTERN: "/no-env/"
  #include this substring in your branch name to suppress environments for that branch.
  #  This is a regex pattern that you can read about here:
  #  https://docs.gitlab.com/ee/ci/jobs/job_control.html#compare-a-variable-to-a-regex-pattern
  TARGET_ENV:  $CI_COMMIT_REF_SLUG-$CI_PROJECT_ID
  # CI_PROJECT_ID is used here to guarantee uniqueness on cloud account level resources
  #  (which use this for named resource uniqueness - for instance the names of
  #  AWS security groups are global for the entire account and so in training and
  #  other highly shared situations, deploying the same named
  #  branch from multiple projects to one cloud account creates a naming conflict.
  #  If unique cloud accounts per GitLab projects are used, then the project id could be removed.

include:
 - template: Jobs/SAST.latest.gitlab-ci.yml
 - template: Jobs/Secret-Detection.latest.gitlab-ci.yml
 - template: Jobs/SAST-IaC.latest.gitlab-ci.yml
 - template: Jobs/Dependency-Scanning.latest.gitlab-ci.yml

stages:
  - build
  - test
  - deploy_function
  - test_deployed_function
  - deploy_pages
  - deploy
  - cleanup

workflow:
  # Rules as documented here: https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: '$CI_COMMIT_BRANCH'

deploy_sls_env:
  stage: deploy_function
  resource_group: $TARGET_ENV
  rules:
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $NO_ENV_BRANCH_PATTERN || $CI_COMMIT_BRANCH =~ $NO_ENV_BRANCH_PATTERN'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: '$CI_COMMIT_BRANCH'    
  before_script:
    - FAILURE_MESSAGE="Must define \$AWS_ACCESS_KEY_ID and \$AWS_SECRET_ACCESS_KEY. Add keys to $CI_PROJECT_URL/-/settings/ci_cd"
    - test -z "$AWS_SECRET_ACCESS_KEY" && echo $FAILURE_MESSAGE && exit 1
    - test -z "$AWS_ACCESS_KEY_ID" && echo $FAILURE_MESSAGE && exit 1
  environment:
    name: $TARGET_ENV
    url: $DYNAMIC_ENVIRONMENT_URL
    action: start
    on_stop: destroy_env
  script:
    - |
      echo "***** Deploying API..."
      npm install
      npm install -g serverless
      serverless deploy --verbose --stage ${TARGET_ENV}
      echo "APP_URL=$(grep -oP '(?<="ServiceEndpoint": ")[^"]*' appstack.json)/hello" >> app_url.env
  artifacts:
    paths:
      - appstack.json
    reports:
      dotenv: app_url.env
    expire_in: 2 weeks

deploy_sls_webui:
  #must pass the api gateway URL as APP_URL
  stage: deploy_function
  resource_group: $TARGET_ENV-webui  
  needs:
    - job: deploy_sls_env
      artifacts: true
  rules:
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $NO_ENV_BRANCH_PATTERN || $CI_COMMIT_BRANCH =~ $NO_ENV_BRANCH_PATTERN'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: '$CI_COMMIT_BRANCH'
  environment:
    name: $TARGET_ENV
    url: $DYNAMIC_ENVIRONMENT_URL
  script:
    - |
      echo "***** Deploying html frontend..."
      cd frontend
      npm install
      npm install -g serverless
      echo "The serverless URL is (passed as APP_URL variable): $APP_URL"
      serverless deploy --verbose --stage ${TARGET_ENV}
      echo "DYNAMIC_ENVIRONMENT_URL=$(grep -oP '(?<="HttpApiUrl": ")[^"]*' ./webstack.json)/webui" >> envurl.env
  artifacts:
    reports:
      dotenv: frontend/envurl.env
    expire_in: 2 weeks

postdeploy_test:
  stage: test_deployed_function
  needs:
    - job: deploy_sls_env
      artifacts: true
  rules:
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $NO_ENV_BRANCH_PATTERN || $CI_COMMIT_BRANCH =~ $NO_ENV_BRANCH_PATTERN'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: '$CI_COMMIT_BRANCH'
  script:
    - npm install
    - STACK_JSON_FILE=./appstack.json npm test featureTests

destroy_env:
  stage: cleanup
  allow_failure: true
  needs:
    - deploy_sls_env
  rules:
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $NO_ENV_BRANCH_PATTERN || $CI_COMMIT_BRANCH =~ $NO_ENV_BRANCH_PATTERN'
      when: never  
    - if: '$CI_COMMIT_REF_PROTECTED == "true"'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
      when: manual    
  environment:
    name: $TARGET_ENV
    action: stop
  script:
    - |
      npm install
      npm install -g serverless
      serverless remove --verbose --stage ${TARGET_ENV}
      cd frontend
      npm install
      npm install -g serverless
      serverless remove --verbose --stage ${TARGET_ENV}