# Tutorial 3.3 - Amazon CodeGuru Secure AI Scanning with Security Policy Approval Rules

## Known Working Version Details

Tested Date: **2023-10-27**

Testing Version (GitLab and Runner): **Enterprise Edition 16.3.0 SaaS**

Report Problems with New Issues here: https://gitlab.com/guided-explorations/aws/serverless/serverless-framework-aws/-/issues

There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** 

- General Requirements for self-paced execution of this tutorial are covered in [SELFPACED-INSTRUCTOR-DEMOER-Requirements.md](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)
- Tutorials 1 and 2 must also be completed and the resultant project available to start this tutorial.

[Authored as Open Educational Resources (OER) and with Hyperscaling Enablement Content Architecture (HECA)](OPEN-EDUCATION-RESOURCES-HECA.md)

## Visual Overview

![Tutorial 1 Visual Overview](images/serverlesstutorial1overview.png)

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
- `[ONLY FOR LEARNING]` = This tag means that for the sake of learning simplicity we doing or showing something that would not be typical in a production environment.
- The labs do not direct you to watch the AWS console activities triggered by GitLab - but you can feel free to watch these events to whatever degree you are familiar with the AWS console.

> Callouts like these will be used to note unique learning points about the steps just completed. 

## Concepts To Watch For

- Amazon CodeGuru Secure Scanning not only integrates with GitLab, it has the full feature set of the built-in scanners because AWS authored their CI integration capability to support GitLab’s security findings reporting format.

## Exercises

### 3.3.1 Update AWS IAM User Permissions

1. Open a browser tab and **login** to the AWS account of your choosing.

2. You should be at the URL https://console.aws.amazon.com 

3. In the Search box **Type** ‘IAM’ and *in the results list*, **Click** “IAM”

4. From the *Identity and Access Management (IAM*) page, on the left nav bar, **Click** ‘Users’

5. On *Users*, under *User name*, **Click** ‘gitlab-serverless-deploy-user’ 

6. On *Add permissions* button, **Click** {the expand down arrow} and **Click** ‘Add Permissions’

7. On *Permissions options*, **Click** ‘Attach policies directly’

8. Under *Permissions policies*, in *Search*, **Type** ‘codegurusecurity’

9. In the filtered policies list, **Select** ‘AmazonCodeGuruSecurityScanAccess’

10. **Click** ‘Next’

11. On *Review*, **Click** ‘Add permissions’

    > Note: You will be sent back to the user details screen where the Permissions policies list should now also display ‘AmazonCodeGuruSecurityScanAccess’

12. Close the IAM console tab.

### 3.3.2 Use Merge Request To See Amazon CodeGuru Security Findings

In this section you will create a GitLab Issue and Merge Request. It is important to work on code using a Merge Request so that we can see all defects associate with just the code we’ve changed on our branch - including security vulnerabilities - which are also ONLY for code we’ve changed or added.

1. While in a browser tab on `full/path/to/yourgroup/serverless-framework-aws`

2. **Click** ‘Plan => Issues => New issue’ (button)

3. On *New Issue*, for *Title (required)*, **Type** ‘Add CodeGuru’

4. **Click** ‘Create issue’

5. On *Updates {new issue view}*, **Click** ‘Create merge request’ 

6. On *New merge request*, **Uncheck** ‘Mark as draft’

7. **Click** ‘Create merge request’

   > **Merge Requests - Developer’s Single Source of Truth and Collaboration**
   > GitLab’s work flow from Issue to Merge Request automatically creates a branch to work on. By initiating this workflow from the issue, there is immediate visibility that work has started and all the artifacts are cleanly linked together. The branch and merge request are already associated and the issue will be closed upon successfully merging. The Merge Request is also the Single Source of Truth (SSOT) for all changes and change approvals for the developer and all collaborators and approvers.
   >
   > When enabled, Merge Requests are also capable of creating an isolated DevOps environment for the code changes - which enables all kinds of CI checks that require a running version of the application, including: DAST Scanning, API Fuzzing, Accessibility Testing, Performance Testing, Browser Testing and any Human QA that is not yet automated.

8. On *Resolve “"Add CodeGuru" {new merge request view}*

9. Find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

   **Note**: A new tab opens with VS Code editing a copy of your project.

   > GitLab supports multiple hosted code editing environments. The WebIDE is a VS Code based editor for all your files. GitPod enables developer environments along with the IDE environment. Single file editing is also available for quick changes.

10. On the left navigation, **Click** ‘.gitlab-ci.yml’

11. **Locate** ‘include:’

12. After the last `- template:` keyword, add the line ` - local: gitlab-ci-libs/awscodeguru.gitlab-ci.yml ` being mindful of using spaces to align with the other references. The result should look like this:

    ```
    include:
      - template: Jobs/SAST.latest.gitlab-ci.yml
      - template: Jobs/Secret-Detection.latest.gitlab-ci.yml
      - template: Jobs/SAST-IaC.latest.gitlab-ci.yml
      - template: Jobs/Dependency-Scanning.latest.gitlab-ci.yml
      - local: gitlab-ci-libs/awscodeguru.gitlab-ci.yml
    ```

13. On the left navigation, **Click** ‘src’ (a subdirectory)

14. Three files will have the extension “unknown” like this:

    ```bash
    src
     bc1.js
     bc2.unknown
     bc3.unknown
     bc4.unknown
    ```

15. Right click each file and rename the extension to `.js` so that it looks like this:

    ```bash
    src
     bc1.js
     bc2.js
     bc3.js
     bc4.js
    ```

16. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

17. In *Commit message*, **Type** ‘Add CodeGuru’

18. **Click** ‘Commit to ’"{number}-add-codeguru"‘

19. **Close** the Web IDE tab.

20. **Click** {the Merge Request tab from which the IDE was launched}

21. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/serverless-framework-aws` and **Click** ‘Code => Merge requests => Resolve “"Add CodeGuru" ’ 

22. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

23. Wait For all jobs to complete successfully - refresh the page as needed. (Pipeline Status should be ‘Passed’).

24. There should be an expandable section for “Security scanning detected…” - if it is not visible or not yet able to be expanded, keep refreshing your browser until the sections are completely populated and expandable.

### 3.3.3 Examining Vulnerability Findings in MR

The following screenshot can be used to understand the next steps in examining the MR findings and approval status.

![amazoncodegurufindings.png](images/amazoncodegurufindings.png)

1. Notice that our security policy is again triggered by the presence of 1 or more Critical findings for SAST - this is indicated by the MR line *Requires 1 approval from Zero Critical for SAST*

2. Next to *Security scanning detected…* , **Click** {the section expand down arrow}

3. Under *SAST findings*, locate three new Critical findings.

4. **Click** ‘CWE-94,95,96,691,705,668,829 - Unsanitized input is run as code’.

5. Notice Scanner Provider is ‘CodeGuru Security Scanner’

6. Notice that File is “bc2.js”

7. In the upper right of the dialog, **Click** ‘x’ to close the finding.

8. **Click** ‘CWE-798,259 - Hardcoded credentials’.

9. Note that CodeGuru reports secrets in SAST results.

10. Next to “File”, **Click** ‘scrl/bc1.js:7’

11. Notice the password is ‘ICanG3tIn’

12. Close the current tab open to the source file - you should be back in the original tab showing the finding, but if not, please switch to that tab.

13. In the upper right of the dialog, **Click** ‘x’ to close the finding.

14. Under Secret detection, **Click** GitLab ‘Personal Access Token’

15. Note that for Tool: it says “Secret Detection” - which means GitLab’s native secret detection scanner.

16. Next to “File”, **Click** ‘scrl/bc1.js:4’

17. Notice that we are on line 4 of the same file we looked at earlier.

    > Amazon CodeGuru Secure found a password that GitLab’s Secret Scanner did not. GitLab’s built-in secret scanner found a GitLab token that Amazon CodeGuru Secure did not. This shows the value of using both GitLab’s built-in scanners and 3rd party scanners together for better coverage.

18. Close the current tab open to the source file - you should be back in the original tab showing the finding, but if not, please switch to that tab.

19. In the upper right of the dialog, **Click** ‘x’ to close the finding.

20. Use this screenshot to explore the security and SBOM MR Widget content:

21. Back in the SAST section, **Click** ‘CWE-384 - Session fixation’

22. Notice Scanner Provider is ‘CodeGuru Security Scanner’

    > All of the Critical SAST results are from Amazon CodeGuru Secure. This means that our GitLab Security Policy Approval Rule was triggered by the 3rd party scan results from Amazon CodeGuru Secure alone.

### 3.3.4 Optional: Resolve CodeGuru Vulnerabilities

> For each of the vulnerabilities found by Amazon CodeGuru, there is also an example of compliant code in the same file. Simply delete the vulnerable version and commit your changes to see the MR be updated with new findings. Once all the findings are resolved, the Merge will be allowed.
