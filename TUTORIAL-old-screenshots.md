
# Tutorial: Serverless Framework Deployment from GitLab to AWS with Security Scanning

## Known Working Version Details
Tested Date: **2023-07-30**

Testing Version (GitLab and Runner): **Enterprise Edition 16.3.0 SaaS**

There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** Requirements for self-paced execution of this tutorial are covered in [SELFPACED-INSTRUCTOR-DEMOER-Requirements.md](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)

## Instructions

Start by creating a new project in the GitLab group of your choosing  (screenshots in this guide use gitlab.com/dsanoy-demo/aws)

![images/createproject.png](images/createproject.png)

**Select** *Import project*

![images/importproject.png](images/importproject.png)

Paste in the repository url: https://gitlab.com/guided-explorations/aws/serverless/serverless-framework-aws.git

![images/importproject2.png](images/importproject2.png)

Once the import of the template is complete, you can then work with the newly created sample project:

![image-20230731153136735](images/newimportedproject.png)

Next step: In new browser tab logged into a non-production AWS account, create an AWS IAM account to use for deployment:

![images/iamconfig1.png](images/iamconfig1.png)

![iamconfig2.png](images/iamconfig2.png)

Attach the necessary policies to the user: 

> NOTE: The included policy is used for convenient setup of this tutorial. Ensure that the policies used in your organization meet your company’s security requirements for deployment (frequently these requirements apply not just to production environments, but also to pre-production, experimental and training environments). The policy for actually running the application is created by the serverless framework when the project runs.

![images/iamconfig3.png](images/iamconfig3.png)

Replace the entire contents of the policy editor with the contents of the project file deploythisserverlessapp.iam.json

![iamconfig4.png](images/iamconfig4.png)

****

![iamconfig5.png](images/iamconfig5.png)

The policy creation will have opened a new tab that you have been working in, click the previous tab in the same browser window (1), then if not already selected, click “Attach policies directly” (2), refresh the policy list (3), type “deploythis” to search (4), select the newly created policy (5) and click next (6)	

![image-20230731170208769](images/iamconfig6.png)

Once these steps are completed, you should have the following summary:

![iamconfig7.png](images/iamconfig7.png)

From the list of users, select the newly created user

![image-20230731173934665](images/keysconfig1.png)

![image-20230731173955561](images/keysconfig3.png)

![image-20230731172342087](images/keysconfig4.png)

![image-20230731172433988](images/keysconfig4b.png)

![image-20230731172653189](images/keysconfig5.png)

You can use the copy controls highlighted above to copy and paste the data into GitLab CI CD variables.

In a new tab or browser windows, navigate to new serverless project: Add the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY as CI/CD variables to the project as shown below: 

> NOTE: Using AWS Keys to authenticate GitLab Runner for CD is used here for convenient setup of this tutorial. GitLab Runner supports 3 types of authentication into AWS environments: AWS Keys, Instance Profiles and OpenID Auth with JWT tokens. Please be sure your AWS Authentication practices match your company’s security requirements.

![image-20230731173851680](images/glcivarsconfig1.png)

Add the variable AWS_ACCESS_KEY_ID 

![image-20230731173829300](images/glcivarsconfig2.png)

Add a second variable as AWS_SECRET_ACCESS_KEY:
![image-20230731173759051](images/glcivarsconfig3.png)

The final variables display should look like this:

![image-20230731174234611](images/glcivarsconfig4.png)

We will now create a new issue and merge request to kick off a pipeline:

![image-20230731175024450](images/mr1.png)

![image-20230731175402874](images/mr2.png)

![image-20230731175504881](images/mr3.png)

![image-20230731175759124](images/mr4.png)



![image-20230731175929837](images/mr5.png)

The project sample contains a .gitlab-ci.yml file. Changes made to the repository, or when a pipeline is initiated, four CI jobs (test, production, postdeploy_test and pages) are run as shown below:

![](images/mr6.png)

Commit the changes:

![image-20230801125420646](images/mr7.png)

![image-20230801073857585](images/mr8.png)

Wait for all jobs to complete successfully (green marks on all jobs):

![image-20230801125541527](images/mr9.png)

Configure On Demand, Per-Vulnerability Security Training
![image-20230807112142090](images/configsecuritytraining.png)

Open the Merge Request to see CI checks and scanning results:

![image-20230801080707757](images/mr10.png)

Take note of the rich scanning information that comes with GitLab’s default scanners. You can extend scanning with any scanner that already produces GitLab’s SAST YAML 

Please note that since this is the first merge request and scanning has never run on the default branch, all existing vulnerabilities in the code show up as though they were just added in this branch.

After this branch is merged and a new one is opened, vulnerabilities in the new merge request will be ONLY for code changes made in the merge request - thereby immediately identifying vulnerabilities as soon as they are added and highlighting them to the person who added them before they merge to a shared branch.

![image-20230807112352939](images/mr11.png)

![image-20230807112618429](images/security1.png)

Once the production job completes, the URL of the Lambda endpoint is created and the GitLab pages functionality is setup with a form to call it. Click the pages url from the project by selecting Deploy => Pages

![image-20230801084326684](images/testapp1.png)

Fill in the form as indicated and click “Run function” to see this output![image-20230801084441712](images/testapp2.png)

You may merge the MR if you wish - this tutorial does not cover that process.
