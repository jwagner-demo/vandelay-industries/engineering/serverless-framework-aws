# Secure Serverless Framework Development with GitLab Security Policy Approval Rules and Managed DevOps Environments

## Known Working Version Details

Tested Date: **2023-10-27**

Testing Version (GitLab and Runner): **Enterprise Edition 16.3.0 SaaS**

Report Problems with New Issues here: https://gitlab.com/guided-explorations/aws/serverless/serverless-framework-aws/-/issues

There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** Requirements for self-paced execution of this tutorial are covered in [SELFPACED-INSTRUCTOR-DEMOER-Requirements.md](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)

[Authored as Open Educational Resources (OER) and with Hyperscaling Enablement Content Architecture (HECA)](OPEN-EDUCATION-RESOURCES-HECA.md)

## Visual Overview

![Tutorial 2 Visual Overview](images/serverlesstutorial2overview.png)

## Conventions and Requirements

- `[ONLY FOR LEARNING]` = This tag means that for the sake of learning simplicity we doing or showing something that would not be typical in a production environment.
- These labs require that you have completed the steps in the first tutorial [Serverless Framework Deployment from GitLab to AWS with Security Scanning](TUTUTORIAL.md)
- The labs do not direct you to watch the creation and destruction of the Lambda functions, applications, API gateway, etc - but you can feel free to watch these events to whatever degree you are familiar with the AWS console.

## Concepts To Watch For

- Many CI systems use ‘build failure’ on shared branches as the primary or exclusive way to force software defects to be dealt with. 
  - There are a number of downsides to this approach if other controls are possible - especially with security vulnerability defects. For instance, having to wait on assistance before any more work can be done. Also, the pain of one-by-one-failures discovery of vulnerabilities.
  - GitLab has very flexible approvals - some of which only become required under the right conditions. Since GitLabs Merge Request process supports this for security vulnerabilities, it prevents a the very disruptive workflow of failing pipelines on vulnerabilities, but still does not allow them to be merged into the next branch. This maximizes developer productivity.
- This tutorial shows the power of GitLab Security Policy approval rules and how they prevent code changes from introducing new vulnerabilities that are not in alignment with your security policies.
- `[ONLY FOR LEARNING]` Remember from the previous tutorial that in the real world (not our test project) - only vulnerabilities that we introduced with our Merge Request code changes would be in the merge request. As we proceed, we’ll be imagining the our own coding activities in our branch are what introduced all the found vulnerabilities.
- You will configure a security policy that will require a new merge approval - in production this would generally be someone from security. The security rule is configured after the findings are found so you can plainly see how this control works.
- Then, as the developer, you will remove the vulnerability instead of having to face the security team and ask for an exception for the critical vulnerability you accidentally introduced in your code. Once the vulnerability is removed, then new approval rule becomes optional.
- Notice that Security professionals only need to review the code and give approval if there is an attempt to merge the vulnerability - otherwise if the developer self-remediates the security defect, no one else needs to be involved.
- **Any third party security scanner can be integrated** into GitLab and have it’s results used in Security Policy approval rules.

## Exercises

### 2.1 Blocking Merges With Security Policies

We will point the security policies configuration back to our same project where a preconfigured rule makes things quicker. In production, this would generally be pointed to a project that contained only security policies and could only be modified by the Security team in your organization.

1. The Tutorial [GitLab Serverless Framework Deployment to AWS with Serverless SAST Scanning and Managed DevOps Environments](TUTORIAL.md) must be completed first so that the project is in the correct state for the following to work. Throughout these exercises this project will be represented as `full/path/to/yourgroup/serverless-framework-aws`

2. While browsing `full/path/to/yourgroup/serverless-framework-aws`, **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

3. Next to *Approval is optional*, **Click** {the section expand down arrow}

   > Notice: There is only one approval rule ‘All eligible users’ and under *Approvals* it says ‘Optional’

4. Next to *Security scanning detected…* , **Click** {the section expand down arrow}

5. There should be only two Critical for SAST titled ‘Critical Serverless Function should encrypt environment variables’

   > You will eventually resolve this vulnerability.

6. On the {left navigation}, ***Click** ‘Settings => Merge requests’ (**NOT** “Code => Merge Requests”)

7. Quite a ways down the page, under *Approvel settings*, if not already done, **Deselect** ‘Prevent approval by author’ 

8. If it is selected, **Deselect** ‘Prevent approvals by users who add commits’

   > `[ONLY FOR LEARNING]` We are doing this ONLY so that we do not need to involve a second GitLab user to demonstrate security policy functionality.

9. Under the section *Approval settings*, **Click** ‘Save changes’ (button) 

   > IMPORTANT: Do not use any other ‘Save changes’ button on the page.

10. On the {left navigation}, ***Click** ‘Secure => Policies’

11. On the *Upper right*, **Click** ‘Edit policy project’

    > Note: You will be pointing this back to the project you are currently in.

12. On the popup window, under *Select security project*, **Click** ‘Select a project’

13. In the pop field *Search your projects*, **Type** ‘Serverless Framework AWS’ (if you changed the name of your project, use the actual name you used)

    > Note - you can hover the names in the filtered list to see their full path and name.

14. In {the selection list}, **Click** {the current project} (if more than one is listed, be careful to select the current project)

15. **Click** ‘Save’

16. The policies list should update with a new policy named ‘Zero Criticals for SAST’

    > `[ONLY FOR LEARNING]` While we have presaged this rule and placed it in the same project, a single GitLab project can house Merge Request Security Policy Rules for many projects. This enables policy based management and ensures developers in a project do not have direct permissions to change the policies they must comply with.

### 2.2 Researching and Resolving Vulnerability To Enable Merging

> Remembering that the developer would have introduced the vulnerabilities in the MR code (rather than having them prestaged). That means that at this point they could either ask for approval from a security approver who can approve critical vulnerabilities - or they can eliminate the vulnerability that they accidentally added to the code.

1. On the {left navigation}, **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

   Note: Now the approvals line read “Requires 1 approval from Zero Critical for SAST”

2. Next to *Requires 1 approval from Zero Critical for SAST*, **Click** {the section expand down arrow}

   > **Security Policies in Action:** You are blocked from merging this code until the two critical SAST findings are resolved. If you resolve them, you can merge without anyone having to ask security for an exception. Remember that in the real world we would have been the author of this vulnerability by some kind of code change we made in this Dev branch + Merge Request.

   > `[ONLY FOR LEARNING]` - normally you would not want the MR author to be able to approve, especially on security policies, but for learning you will likely see only your avatar for approvers.

3. Next to *Security scanning detected…* , **Click** {the section expand down arrow}

4. Under *SAST findings*, locate *Critical Serverless Function should encrypt environment variables* and **Click** ‘Serverless Function should encrypt environment variables’

5. Next to *Identifiers*, **Click** ‘Serverless Function Environment Variables Not Encrypted’

   Note: A new page opens on a new tab.

   > The [linked page](https://www.serverless.com/framework/docs/providers/aws/guide/functions#kms-keys) explains that `serverless.yml` needs to specify `kmsKeyArn:` to prevent the parameters from being vulnerable.

6. Close the current tab (viewing the “serverless” site) and switch back to the last browser tab where the Merge Request view is.

7. In the vulnerability pop-up dialog, next to *File*, **Click** ‘serverless.yml:4’ (4 is the line number and might be slightly different for you)

   > A new browser tab opens to the function name “provider:” section in serverless.yml so that you are able to locate the specific location where the code needs to be updated.

8. Close the current tab (viewing the code file) and switch back to the last browser tab where the Merge Request view is.

9. In the vulnerability pop-up dialog, to close the popup, **Click** {the “x” in the upper left corner}

10. Near the top right of the Merge Request, find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

    **Note**: A new tab opens with VS Code editing a copy of your project.

11. Open a new browser tab or window to https://console.aws.amazon.com 

    > You will now retrieve a preexisting KMS Key ARN to use in the code.

12. Login to the AWS account you created the keys in the previous tutorial.

13. Next to *Services*, in the Search box **Type** ‘KMS’

14. *In the results list*, **Click** “Key Management Service”

15. Be sure you are in the region from your serverless.yml (should be us-east-1 if you deployed with the default instructions) - the upper right of the screen shows the current region.

16. On the {left navigation}, Under *Key Management Service (KMS)*, **Click** ‘AWS managed keys’

17. Under the column heading *Aliases*, **Click** ‘aws/lambda’

18. Under *General configuration*, below *ARN*, **Click** {the copy icon which looks like a two document stack}

19. Switch to the tab where you have the GitLab Web IDE open on your Merge Request branch (be sure you are in the right Web IDE if you have more than one open) If you are unsure, find the Merge Request and launch the Web IDE from it again.

20. In *The Web IDE*, on {the left side file navigation}, **Click** ‘serverless.yml’

21. Find the text `name: aws:`

22. Insert a line immediately under it.

23. **Type** ‘kmsKeyArn: ’ (retain the exact capitalization shown here as well as exact spaces before the keyword)

24. Paste {they kms key ARN in your copy buffer} (If it is no longer there, find the tab with the KMS Key and copy the ARN again.)

    Final result should look like this, but with a different arn value (fragment depicted):

    ```yaml
    provider:
      name: aws
      kmsKeyArn: arn:aws:kms:us-east-1:111111111111:key/6c383cd7-770e-4f15-a7fa-d1a9bcbdf2cc
      region: ${env:AWS_REGION}
    ```

25. **IMPORTANT:** Complete the same above five file editing steps with the same KMS Key Arn for the file “frontend/serverless.yml”

26. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

27. In *Commit message*, **Type** ‘Add KMS arn’

28. Under the commit message, **Click** ‘Commit to ’1-updates’‘

29. After a successful commit, **Close** the ‘Web IDE browser’ tab and return to the MR tab.

30. **Click** {the Merge Request tab from which the IDE was launched}

31. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/serverless-framework-aws` and **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

32. Refresh the browser tab for updated statuses.

33. Near the top, next to *Merge request pipeline #1111111111111 running*, watch the job status bubbles.

34. Wait For all jobs on the latest pipeline to complete successfully. (You may need to refresh your browser occasionally)

35. Refresh the browser tab for updated merge approval statuses until the pipeline status near the top of the MR is completed.

36. After all jobs have finished, the Approval line should now read Approval is optional, **Click** {the section expand down arrow}

37. Notice: that for the rule *Zero Criticals for SAST*, the *Approvals* column says ‘Optional’

    > The “Zero Criticals for SAST” rule will now be optional because you no longer have any Critical findings and so meet the companies security policy for new code vulnerabilities.

38. Next to *Security scanning detected…* , **Click** {the section expand down arrow}

    > Notice that the ‘Critical Serverless Function should encrypt environment variables’ has disappeared.

> Observation: You have just walked through how GitLabs Empowered Shift Left instills new habits by showing new findings that have been introduced by coding activities while the developer is still working to get their overall functionality working and how they can be blocked from merging (without failing security scan jobs as a gate mechanism)

### 2.3 Testing Deployed API

1. While in a browser tab on `full/path/to/yourgroup/serverless-framework-aws`

2. **Click** ‘Operate => Environments’

3. To the right of *1-updates-{number}* **Click** ‘Open’

   > Notice the top of the page says ‘Environment: 1-updates-{number}’

4. On the new tab, for *Param value*, **Type** ‘More secure’

5. **Click** ‘Run function’

6. Under *Function Output* {review the function’s json return value}

7. Close the current tab.

### 2.4 Merging To See MR Lifecycle DevOps Environment Removed

1. While browsing `full/path/to/yourgroup/serverless-framework-aws`, **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

2. Under *Ready to merge!* **Select** ‘Delete source branch’ (default)

3. **Click** “Merge” (blue button)

4. Near the top of the page body, notice that just one of the stages in “Merge request pipeline” is processing.

   > This is the cleanup job removing the DevOps Review Environment for our branch and merge request.

5. Refresh the page until that job is complete.

6. On the left navigation, **Click** ‘Build => Pipelines’

   > This pipeline will deploy to the production environment because it on the default branch for the repository.

7. Wait for the latest pipeline to complete (you may need to refresh the page a few times).

8. On the left navigation, **Click** ‘Operate => Environments’

   > Notice that the environment ‘1-updates-{number}’ is gone and there is a new environment called ‘prod-{number}’

9. To the right of *prod-{number}* **Click** ‘Open’

   > Notice the top of the page says ‘Environment: prod’

10. On the new tab, for *Param value*, **Type** ‘Production Deployed!’

11. **Click** ‘Run function’

12. Under *Function Output* {review the function’s json return value}

13. Optional: If you have access to the AWS console and deployed to us-east-1, you can use this link to see that your branch named Lambda application is gone and a new one named after the default branch is now present: https://us-east-1.console.aws.amazon.com/lambda/home?region=us-east-1#/applications

14. Close the current tab and switch back to the last browser tab where the Environment view is.

### 2.5 Use Security Dashboards To See Vulnerabilities in Default Branch (and Production Environment) and Dependencies SBOM and Licensing SBOM

This section shows some of the GitLab Security Dashboards. These dashboards track the vulnerabilities that are in the code on the default branch. They may be there because new CVEs have been issued for vulnerabilities that were not known at the last time you did a build or because you chose to allow them into the software according to your security policies about vulnerability management.

GitLab Security Dashboards are roughly equivalent to traditional security tool scanning that happens on the entire code base (but NOT shifted left like the MR Developer Experience). This capability is needed for a wholistic approach to vulnerability management.

1. While in a browser tab on `full/path/to/yourgroup/serverless-framework-aws`

2. **Click** ‘Secure => Vulnerability report’

   **NOTE: Each group level above your current project also has the Vulnerability report dashboard and it shows vulnerabilities for ALL child projects to enable team and organization level management of vulnerabilities.**

   > These vulnerabilities were accepted into the default branch and production by the Merge Approval process we just completed. This report allows the creation of issues to pay down security debt in future sprints. Dependency vulnerabilities are listed here.
   >
   > To learn more see the [Vulnerability report documentation](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/).

3. **Click** ‘Secure => Dependent list’

   **NOTE: Each group level above your current project also has the Dependency list dashboard and it shows Dependencies for ALL child projects to enable team and organization level management of dependencies.**

   > This is a complete list of the dependencies in your software - which is part of the Software Bill of Materials (SBOM) and it also notes which ones are vulnerable.
   >
   > To learn more see the [Dependency list documentation](https://docs.gitlab.com/ee/user/application_security/dependency_list/).

4. **Click** ‘Secure => License compliance’

   > This is a complete list of licenses in your software - also part of the Software Bill of Materials (SBOM). If we had license policies configured, non-compliant licenses would be flagged. This helps developers notice when licenses of dependencies change from acceptable licenses to undesirable licenses.
   >
   > To learn more see the [License list documentation](https://docs.gitlab.com/ee/user/compliance/license_list.html).

5. **Click** ‘Secure => On-demand scans’

   > This is where you setup scanning of code bases that do not get built very often - so that you can find new vulnerabilities in code that changes infrequently.


**IMPORTANT:** The vulnerabilities that are now in the Security Dashboard will not show in any Merge Requests (including in the next lab) because Merge Requests only show vulnerabilities for code you change in your branch. Our first lab was non-real world in that we purposely created a Merge Request as if we had just created all the code in the project so that we could see the extent of scanning available.

### 2.6 Create New Merge Request To Understand Per-Dev Branch Managed DevOps Environments and Differential Branch Vulnerabilities in MRs

In this section you will observe that a change in background color is visible in your new MR review environment and then upon merge your review environment is destroyed and the production environment is updated.

1. While in a browser tab on `full/path/to/yourgroup/serverless-framework-aws`

2. **Click** ‘Plan => Issues’

3. On the upper right, **Click** ‘New issue’ (button)

4. On *New Issue*, for *Title (required)*, **Type** ‘New Color’

5. **Click** ‘Create issue’

6. On *Updates {new issue view}*, **Click** ‘Create merge request’ 

7. On *New merge request*, **Uncheck** ‘Mark as draft’

8. Leave all other settings at their defaults

9. At the bottom of the page, **Click** ‘Create merge request’

10. On *Resolve “New Color” {new merge request view}*

11. Find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

   > **Note**: A new tab opens with VS Code editing a copy of your project.

11. On the left navigation, **Click** ‘frontend’ (folder) and then ‘handler.js’

12. **Locate** ‘<body bgcolor="white">’

13. Change `white` to `lightgreen`

14. It should look like this:

    ```html
    <body bgcolor="lightgreen"> 
    ```

    > You will now add back a vulnerability to simulate the accidental introduction of a vulnerability in your MR code.

15. On {the left side file navigation}, **Click** ‘serverless.yml’ (at the root of the project, not under “frontend” folder)

16. Find the text `kmsKeyArn:`

17. Delete the entire line. The result should look like this (a line is removed)

    ```yaml
    provider:
      name: aws
      region: ${env:AWS_REGION}
      runtime: nodejs18.x
    ```

18. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

19. In *Commit message*, **Type** ‘Change color’

20. **Click** ‘Commit to ’2-new-color’

21. Close the current Web IDE tab and switch back to the last browser tab where the Merge Request view is.

22. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/serverless-framework-aws` and **Click** ‘Code => Merge requests => Resolve “New Color” ’ 

23. Watch the pipeline status at the top of the Overview section. It starts with “Merge request pipeline #111111111 running”

24. Wait For all jobs in the new pipeline to complete successfully (Pipeline Status will be ‘Passed’).

    > If the pipeline does not seem to be progressing, use the left navigation menu “Build => Pipelines” to view the status.

25. Refresh the page.

    > **Empowered Shift Left:** Notice that merging is again blocked by the Merge Approvals policy and that the number of findings is much smaller because it is only picking up on the new findings - in this case the fact that you no longer have a kms key associated with the function. This is the default mode of MR based scanning - finding new vulnerabilities accidentally added during the code changes on the current branch.

    ![image-20230906120911790](images/secondmr.png)

26. Just under the heading *Merge request pipeline 11111111 passed*, **Click** ‘View app’ (button) which takes you directly to the DevOps Review Environment.

    > Notice that the background is green and the title is “Environment: pr2-new-color-number”

27. Switch tabs to be browsing your project `full/path/to/yourgroup/serverless-framework-aws`

28. **Click** ‘Operate => Environments’

    > Notice you now have one environment for prod and one for your current merge request / development branch.

29. You just recently saw that your development environment has a green background.

30. To the right of *prod-{number}* **Click** ‘Open’

    > Notice the top of the page says ‘Environment: prod’ and the background is white.

31. Switch back to the tab with the environments page and to the right of ‘2-new-color-number’ **Click** open to compare.

    > Now you will merge to the default branch to see the destruction of the Managed DevOps Environment for your MR branch and that your changes also update the Managed DevOps Environment for production. In approving the merge you are also emulating what the Security Team would have to do if they were approving an exception to this Merge Approval Policy.

32. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/serverless-framework-aws` and **Click** ‘Code => Merge requests => Resolve “New Color” ’

33. Right before *Requires 1 approval from Zero Criticals for SAST*  **Click** ‘Approve’ (blue button)

34. Under *Ready to merge!*, **Click**, ‘Merge’ (blue button)

35. Directly under where you pressed the button, you can see the status of the pipeline merging to prod, wait for it to complete (refresh if it does not appear right away).

    > If the pipeline does not seem to be progressing, use the left navigation menu “Build => Pipelines” to view the status.

36. **Click** ‘Operate => Environments’

    > Notice that the review environment is gone.

37. To the right of *prod-{number}* **Click** ‘Open’

    > Notice that the background is now green, which indicates the changes we saw in our review environment are now in production. Note that sometimes it can take a while for the ASG to replace all instances, so your changes may not be visible for a while.

    > Optional: If you have access to the AWS console and deployed to us-east-1, you can use this link to see your new Lambda Application: https://us-east-1.console.aws.amazon.com/lambda/home?region=us-east-1#/applications

### What Was Observed And Learned

Review “Concepts to Watch for” and reflect on how each of these items was seen in the lab work.