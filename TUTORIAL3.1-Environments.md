# TUTORIAL 3.1: Serverless Framework to AWS with Long Lived Shared stage Branch Environment

## Known Working Version Details

Tested Date: **2023-08-21**

Testing Version (GitLab and Runner): **Enterprise Edition 16.4.0 SaaS**

Report Problems with New Issues here: https://gitlab.com/guided-explorations/aws/terraform/terraform-web-server-cluster/-/issues

There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** 

- General Requirements for self-paced execution of this tutorial are covered in [SELFPACED-INSTRUCTOR-DEMOER-Requirements.md](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)
- Tutorials 1 and 2 must also be completed and the resultant project available to start this tutorial.

[Authored as Open Educational Resources (OER) and with Hyperscaling Enablement Content Architecture (HECA)](OPEN-EDUCATION-RESOURCES-HECA.md)

## Visual Overview

![serverlesstutorial3gitlabworkflow](images/serverlesstutorial3gitlabworkflow.png)

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
- `[ONLY FOR LEARNING]` = This tag means that for the sake of learning simplicity we doing or showing something that would not be typical in a production environment.
- The labs do not direct you to watch the AWS console activities triggered by GitLab - but you can feel free to watch these events to whatever degree you are familiar with the AWS console.

## Concepts To Watch For

- Simply by adding a new, long lived branch called ‘stage’ and protecting it, we can alter the Git Workflow to enable a long lived pre-production environment for integrating multiple branches into a production release.

## Exercises

### 3.1.1 Creating and Configuring a Long Lived stage Branch

1. The Tutorial [Terraform Deployment to AWS with GitLab IaC SAST Scanning](TUTORIAL.md) must be completed first so that the project is in the correct state for the following to work. Throughout these exercises this project will be represented as `full/path/to/yourgroup/terraform-web-server-cluster`

2. While browsing `full/path/to/yourgroup/terraform-web-server-cluster`, on the left navigation **Click** ‘Code => Branches ” ’ 

3. In the upper right **Click** ‘New branch’

6. In *Branch name*, **Type** ‘stage’ and **Click** ‘Create branch’

7. On the left navigation, **Click** ‘Settings => Repository’ (**NOT** ‘Code => Repository’)

8. Next to *Branch defaults*, **Click** ‘Expand’

9. Under *Default branch*, **Select** ‘stage’

10. At the bottom of the expanded Branch defaults section, **Click** ‘Save changes’

    > The section will collapse.

11. Next to *Protected branches*, **Click** ‘Expand’

12. Near the upper right of the expanded section, **Click** ‘Add protected branch’

13. In the *Protect a branch* form, for *Branch*, **Select** ‘stage’

14. In *Allowed to merge*, **Select** ‘Developers + Maintainers’

15. In *Allowed to push and merge*, **Select** ‘No one’

16. **Click** ‘Protect’

17. In the list of protected branches, for *Prod*, under *Allowed to push and merge*, **Select** ‘No one’

### 3.1.2 Observing the New Long Lived Environment

1. While browsing `full/path/to/yourgroup/terraform-web-server-cluster`, on the left navigation **Click** ‘Build => Pipelines ’

2. All pipelines should have completed successfully, if they have not, wait for completion (or resolve problems if any have occurred).

3. Once all pipelines have completed successfully, on the right navigation, **Click** ‘Operate => Environments’

   > There should be two environments, one starting with “prod-” and one starting with “stage-”

4. On each environment line, **Click** ‘Open’ to see the environments are separate.

   > Optional: If you have access to the AWS console and deployed to us-east-1, you can use these links to see stack resources for both environments. The new ones should start with `prod`: https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#Instances, https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#SecurityGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#AutoScalingGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#LoadBalancers

### 3.3 Implied New Git Workflow

With stage identified as a the default branch and protected, the following Git Workflow would now be typical.

1. Create new merge requests from new issues, 

   > This will create new feature branches from ‘stage’ (from branch will now be ‘stage’ because it was made the default branch) 
   > => **This results in a new MR Lifecycle Branch and Environmen**t.

2. Merge multiple completed feature branches back to ‘stage’ 

   > The default merge target branch will now be ‘stage’ because it was made the default branch. Merge conflicts between parallel work are resolved at this time. These MRs default to “Delete” the source branch which is critical for MR Environment environment cleanup.

3. When ready to release the candidate release that has been accumulating in the stage branch, Create a merge request that targets ‘prod’

   > Since ‘stage’ is protected, the branch “Delete” option will not be available - which is good because it is meant to be long lived. This merge request is purely for GitOps environment deployment. Maintainers and higher scan still stop the environment from the “Operate => Environments” menu which might be needed to completely reset a bad environment.

4. Create a Git tag and GitLab Release for the merged commit in the prod branch.

As shown in this visual we’ve just moved prod to be a deployment only branch and created a stage branch for direct feature merge.

![serverlesstutorial3gitlabworkflow](images/serverlesstutorial3gitlabworkflow.png)

### 3.1.4 As Many Workflow or Experimental Environments As Needed

Some organizations may insert additional long lived branches and environments in their standard workflow.

An additional need is experimental environments that will never be merged. Sometimes an environment needs to be created to test upgrade scenarios or an older customer environment. In this project you can experiment with creating a branch called “experiment1” and pretend you are going to make some direct changes the cloud operating environment. Once you have your information, you can directly dispose of the environment by simply deleting the branch.

The below diagram shows what we’ve covered so far but adds an integration environment (“integ”) to the workflow.

It also shows using branches just to create disposable experimental environments as a form of self-service IT. This example can currently do these self-service environments via branches. Notice one of the experiments is checking out an old version (v1.2) of the code to setup a previous instance of the environment and application to experiment on something. This would be creating a branch from a production release tag.

![serverlesstutorial3gitlabenvironments](gitlabbranchlifecycledevopsenvironments.png)

### 3.1.5 Optional: Create an “experiment1” Environment

1. While browsing `full/path/to/yourgroup/terraform-web-server-cluster`, on the left navigation **Click** ‘Code => Branches ” ’ 

2. For *Branch name*, **Type** ‘experiment1’ and **Click** ‘Create branch’

3. On the left navigation **Click** ‘Code => Branches ” ’ 

4. In the upper right **Click** ‘New branch’

5. In *Branch name*, **Type** ‘experiment1’ and **Click** ‘Create branch’

   > Notice the branch source defaults to ‘stage’ since the last project set it to be the default.

6. When the pipeline for ‘experiment1’ is done running, use “Operate => Environments” to visit the environment homepage.

7. To remove the environment, **Click** ‘Code => Branches ” ’

8. To the far right of the branch name, click the three dots and **Select** ‘Delete branch’

> Protected environments (and any other environment) can be stopped by Maintainers and higher from the “Operate => Environments” page

